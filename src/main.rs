use lazy_static::*;
use std::collections::HashMap;
use yew::prelude::*;

lazy_static! {
    static ref PRODUCTS: HashMap<String, Product> = {
        [
            Product::new("Chicken Suppe",   24, "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fcrepesteahouse.com%2Fwp-content%2Fuploads%2F2016%2F09%2Fsoup-chicken.png&f=1&nofb=1"),
            //Product::new("Oreo Milkshake", 32, "https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2Fbk-emea-prd.s3.amazonaws.com%2Fsites%2Fburgerking.pt%2Ffiles%2FMILKSHAKE-OREO.png&f=1&nofb=1"),
            Product::new("Lasagne", 29, "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fd37rttg87jr6ah.cloudfront.net%2Fstatic%2Fproduct_photo_web%2Fgrass_fed_beef_lasagna-2017-01-12-16-32-46_1242x1242.png&f=1&nofb=1"),
            Product::new_vegan("LAsagne", 29, "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fwww.ketomei.com%2Fwp-content%2Fuploads%2F2020%2F06%2FBeef-Lasagna-W.png&f=1&nofb=1"),
            Product::new_vegan("Tomat suppe", 20, "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fwww.innit.com%2Fmeal-service%2Fen-US%2Fimages%2FMeal-pureed_soups%253A%2520c-puree_soup_base-tom_bas%252Bc-topping-her_goa_che_1545372525909_960x960.png&f=1&nofb=1"),
        ].iter().map(|prod|(prod.name.clone(), prod.clone())).collect()
    };
}

type Pin = fn() -> Html;
const VEGAN_PIN: Pin = || html! {<i class="fas fa-leaf"></i>};

#[derive(Hash, Clone)]
struct Product {
    image: String,
    name: String,
    price: usize,
    pins: Vec<Pin>,
}

#[derive(Clone, Copy, PartialEq)]
enum Page {
    Cart,
    Home,
    Checkout,
}

impl Product {
    fn new(name: &str, price: usize, image: &str) -> Self {
        Self {
            name: name.to_string(),
            image: image.to_string(),
            price,
            pins: vec![],
        }
    }
    fn new_vegan(name: &str, price: usize, image: &str) -> Self {
        Self {
            name: name.to_string(),
            image: image.to_string(),
            price,
            pins: vec![VEGAN_PIN],
        }
    }

    fn pins(&self) -> impl Iterator<Item = Html> + '_ {
        self.pins.iter().map(|p| p())
    }
}

enum Msg {
    AddToCart(String),
    RemoveFromCart(String),
    Cart,
    Home,
    Checkout,
}

#[derive(Clone)]
struct Model {
    // `ComponentLink` is like a reference to a component.
    // It can be used to send messages to the component
    link: ComponentLink<Self>,
    items_in_cart: usize,
    cart: HashMap<String, usize>,
    page: Page,
}

impl Component for Model {
    type Message = Msg;
    type Properties = ();

    fn create(_props: Self::Properties, link: ComponentLink<Self>) -> Self {
        Self {
            link,
            items_in_cart: 0,
            cart: HashMap::new(),
            page: Page::Home,
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::AddToCart(prod) => {
                self.items_in_cart += 1;
                if let Some(n) = self.cart.get_mut(&prod) {
                    *n += 1;
                } else {
                    self.cart.insert(prod, 1);
                };
            }
            Msg::RemoveFromCart(prod) => {
                self.items_in_cart -= 1;
                if self.cart[&prod] > 1 {
                    *(self.cart.get_mut(&prod).unwrap()) -= 1;
                } else {
                    self.cart.remove(&prod);
                }
            }
            Msg::Home => self.page = Page::Home,
            Msg::Cart => self.page = Page::Cart,
            Msg::Checkout => {
                self.cart = HashMap::new();
                self.items_in_cart = 0;
                self.page = Page::Checkout;
            }
        }
        true
    }

    fn change(&mut self, _props: Self::Properties) -> ShouldRender {
        // Should only return "true" if new properties are different to
        // previously received properties.
        // This component has no properties so we will always return "false".
        false
    }

    fn view(&self) -> Html {
        match self.page {
            Page::Cart => self.cart(),
            Page::Checkout => self.checkout(),
            Page::Home => self.home(),
        }
    }
}

impl Model {
    fn nav_bar(&self) -> Html {
        html! {
            <nav>
                <ul>

                <img class="logo" src="https://www.dropbox.com/s/x34t90edz1t4xnt/logo_new.png?dl=1"/>

                <li><button class={if self.page == Page::Home{"on-tab"}else{""}} onclick=self.link.callback(|_| Msg::Home)>
                <i class="fas fa-store"></i>
                </button></li>

                //<li class="nav-arrow"><i class="fas fa-arrow-right"></i></li>

                <li><button class={if self.page == Page::Cart{"on-tab"}else{""}} onclick=self.link.callback(|_| Msg::Cart)><i class="fas fa-shopping-cart"></i>
                <b><div class="cart-count">{if self.items_in_cart > 9{"+".to_string()}else if self.items_in_cart == 0{"".to_string()}else{self.items_in_cart.to_string()}}</div></b>
                </button></li>

                //<li class="nav-arrow"><i class="fas fa-arrow-right"></i></li>

                <li><button class={if self.page == Page::Checkout{"on-tab"}else{""}} onclick=self.link.callback(|_| Msg::Checkout)>
                <i class="fas fa-receipt"></i>
                </button></li>

                <h1 class="total"> {if self.items_in_cart > 0 && self.page == Page::Cart{
                    format!("{}kr", self.cart.iter().map(|(prod, count)| PRODUCTS[prod].price * count).sum::<usize>())
                }else{"".to_string()}} </h1>

                </ul>
            </nav>
        }
    }

    fn cart(&self) -> Html {
        let mut products = vec![];
        for (prod, count) in &self.cart {
            let prod = prod.clone();
            let count = count.clone();
            let name = &PRODUCTS[&prod].name;
            let price = format!("{}kr", PRODUCTS[&prod].price);
            let image = PRODUCTS[&prod].image.clone();

            products.push(html! {
                <div class="product-card">
                    <h1 class="product-name">{name} {format!(" ({})",count)}</h1>
                    <div class="product-price">{price}</div>
                        <button class="remove" onclick=self.link.callback(move |_| Msg::RemoveFromCart(prod.clone()))>
                            {"REMOVE ITEM"}
                        </button>
                    <img class="product-image" src={image}/>
                </div>
            });
        }

        html! {
            <div>
                {self.nav_bar()}
                {if self.items_in_cart == 0{html!{
                    <h3 class="empty">{"your cart is empty"}</h3>
                }}else{html!{}}}
                {for products}
                {if self.items_in_cart > 0{html!{
                    <li><button class="to-checkout" onclick=self.link.callback(|_| Msg::Checkout)>
                        <i class="fas fa-check"></i>
                    </button></li>
                }}else{html!{}}}
            </div>
        }
    }

    fn home(&self) -> Html {
        let products = PRODUCTS.iter().map(|(_, prod)| {
        let name = &prod.name;
        let price = format!("{}kr", prod.price);
        let image = prod.image.clone();

            html! {
                <div class="product-card">
                    <h1 class="product-name">{name}</h1>
                    <div class="pins">{for prod.pins()}</div>
                    <div class="product-price">{price}</div>
                        <button class="buy" onclick=self.link.callback(|_| Msg::AddToCart(prod.name.clone()))>
                            <i class="fas fa-shopping-basket"></i>
                            {
                                if self.cart.iter().any(|product| product.0 == name)
                                {format!(" ADD MORE ({})", self.cart[name])}
                                else{" ADD TO CART ".to_string()}
                            }
                        </button>
                    <img class="product-image" src={image}/>
                </div>
            }
        });

        html! {
            <div>
                {self.nav_bar()}
                {for products}
            </div>
        }
    }

    fn checkout(&self) -> Html {
        html! {
            <div>
                {self.nav_bar()}
                <center><h3>{"Thank you for shopping with Cheap Frost 🎉"}</h3></center>
                //<center><h3>{"Thank you for shopping with"}<b>{" Cheap Frost 🎉"}</b></h3></center>
            </div>
        }
    }
}

fn main() {
    yew::start_app::<Model>();
}
